#!/bin/bash

echo ${ROOT_CA_CERTIFICATE} | base64 -d - >> /usr/local/share/ca-certificates/Example_Root_CA.crt
echo ${INTERMEDIATE_CA_CERTIFICATE} | base64 -d - >> /usr/local/share/ca-certificates/Example_Intermediate_CA.crt
update-ca-certificates
