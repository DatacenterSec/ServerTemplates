#!/bin/bash

set -e

CERTIFICATE_AUTHORITY=$(curl -H "Content-Type: application/json" -H "Authorization: Bearer ${DIGITALOCEAN_READONLY_API_TOKEN}" "https://api.digitalocean.com/v2/droplets?tag_name=certificate-authority" | jq '.droplets[0].networks.v4[] | select(.type == "public") | .ip_address' | tr --delete '"')

while [ -z ${CERTIFICATE_AUTHORITY} ]
do
  sleep 3
  CERTIFICATE_AUTHORITY=$(curl -H "Content-Type: application/json" -H "Authorization: Bearer ${DIGITALOCEAN_READONLY_API_TOKEN}" "https://api.digitalocean.com/v2/droplets?tag_name=certificate-authority" | jq '.droplets[0].networks.v4[] | select(.type == "public") | .ip_address' | tr --delete '"')
done

sed -i '/certificate.authority/d' /etc/hosts
echo ${CERTIFICATE_AUTHORITY} certificate.authority | tee -a /etc/hosts
