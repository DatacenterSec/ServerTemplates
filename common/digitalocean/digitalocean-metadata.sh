#!/bin/bash
set -e

cat << EOF | sudo tee /run/digitalocean-metadata
DIGITALOCEAN_REGION=$(curl -s http://169.254.169.254/metadata/v1/region)
DIGITALOCEAN_HOSTNAME=$(curl -s http://169.254.169.254/metadata/v1/hostname)
DIGITALOCEAN_PRIVATE_IPV4_ADDRESS=$(curl -s http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address)
DIGITALOCEAN_PUBLIC_IPV4_ADDRESS=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
EOF
