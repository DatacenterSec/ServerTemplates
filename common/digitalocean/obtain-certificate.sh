#!/bin/bash

set -e

# Wait until certificate.authority is resolvable.

CA_SERVER=$(dig +short certificate.authority)

while [ -z ${CA_SERVER} ]
do
  sleep 1
  CA_SERVER=$(dig +short certificate.authority)
done

# Wait until certificate.authority is available.

until curl -X POST --data '{}' --silent --fail https://certificate.authority/api/v1/cfssl/info; do sleep 1; done

cd /etc/ssl
rm -f cert.pem cert.csr cert-key.pem
cfssl gencert -config=/etc/ssl/cfssl-config.json csr.json | cfssljson -bare
chmod 0644 cert-key.pem
