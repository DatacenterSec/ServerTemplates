storage "consul" {
  scheme        = "https"
  tls_key_file  = "/etc/ssl/cert-key.pem"
  tls_cert_file = "/etc/ssl/cert.pem"
  tls_ca_file   = "/usr/local/share/ca-certificates/Example_Intermediate_CA.crt"
  path          = "vault/"
}

listener "tcp" {
  address                            = "0.0.0.0:8200"
  tls_key_file                       = "/etc/ssl/cert-key.pem"
  tls_cert_file                      = "/etc/ssl/cert.pem"
  tls_client_ca_file                 = "/usr/local/share/ca-certificates/Example_Intermediate_CA.crt"
  tls_require_and_verify_client_cert = "true"
}
