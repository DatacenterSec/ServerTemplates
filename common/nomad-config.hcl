data_dir      = "/var/lib/nomad"
enable_syslog = true

advertise {
  http = "{{ GetInterfaceIP \"eth1\" }}"
  rpc  = "{{ GetInterfaceIP \"eth1\" }}"
  serf = "{{ GetInterfaceIP \"eth1\" }}"
}

consul {
  address   = "127.0.0.1:8500"
  ssl       = true
  key_file  = "/etc/ssl/cert-key.pem"  
  cert_file = "/etc/ssl/cert.pem"
  ca_file   = "/usr/local/share/ca-certificates/Example_Intermediate_CA.crt"
}

tls {
  http = true
  rpc  = true

  key_file  = "/etc/ssl/cert-key.pem"  
  cert_file = "/etc/ssl/cert.pem"
  ca_file   = "/usr/local/share/ca-certificates/Example_Intermediate_CA.crt"

  # We cannot both have HTTPS health checks and verify the client.
  # So we have to sacrifice HTTPS health checks because we need the client verification.
  # https://github.com/hashicorp/nomad/commit/77d9b417c1a18320f4cd7606c47700fbac3f2199
  
  verify_https_client    = true
  verify_server_hostname = true
}

vault {
  enabled     = true
  key_file    = "/etc/ssl/cert-key.pem"
  cert_file   = "/etc/ssl/cert.pem"
  ca_file     = "/usr/local/share/ca-certificates/Example_Intermediate_CA.crt"

  create_from_role = "nomad-cluster"
}
