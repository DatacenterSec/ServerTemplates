#!/bin/bash

set -e

gpg --import /tmp/hashicorp.asc
curl -Os https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_SHA256SUMS
curl -Os https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_SHA256SUMS.sig
curl -Os https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip
gpg --verify consul_${CONSUL_VERSION}_SHA256SUMS.sig consul_${CONSUL_VERSION}_SHA256SUMS
sha256sum --check --ignore-missing consul_${CONSUL_VERSION}_SHA256SUMS
unzip -qq consul_${CONSUL_VERSION}_linux_amd64.zip -d /usr/bin
rm -f consul_${CONSUL_VERSION}_SHA256SUMS consul_${CONSUL_VERSION}_SHA256SUMS.sig consul_${CONSUL_VERSION}_linux_amd64.zip
echo "CONSUL_HTTP_ADDR=https://localhost:8500" >> /etc/environment
echo "CONSUL_CLIENT_KEY=/etc/ssl/cert-key.pem" >> /etc/environment
echo "CONSUL_CLIENT_CERT=/etc/ssl/cert.pem" >> /etc/environment
consul version
systemctl daemon-reload
systemctl reenable consul
sync
