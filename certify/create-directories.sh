#!/bin/bash

set -e

mkdir -p /etc/cfssl
chown --recursive ca:ca /etc/cfssl

mkdir -p /etc/inspec
mkdir -p /etc/iptables
