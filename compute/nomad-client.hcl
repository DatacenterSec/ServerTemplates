client {
  enabled           = true

  options = {
    "driver.whitelist" = "docker"
    "driver.docker" = 1
    "driver.docker.version" = "18.06.1-ce"
  }
}
