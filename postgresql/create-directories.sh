#!/bin/bash

set -e

sudo mkdir -p /etc/inspec
sudo mkdir -p /etc/iptables
sudo mkdir -p /etc/consul
sync
