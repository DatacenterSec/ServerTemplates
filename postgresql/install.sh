#!/bin/bash

set -e

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" | sudo tee /etc/apt/sources.list.d/postgresql.list
sudo apt update
sudo apt install -yq postgresql-11
sudo systemctl stop postgresql
sudo rm -rf /var/lib/postgresql/11/main
sudo rm -rf /lib/systemd/system/postgresql@.service
sync
