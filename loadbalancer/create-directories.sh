#!/bin/bash

set -e

mkdir -p /etc/consul
mkdir -p /etc/haproxy
mkdir -p /etc/inspec
mkdir -p /etc/iptables
