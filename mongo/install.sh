#!/bin/bash

set -e

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 68818C72E52529D4
echo "deb http://repo.mongodb.org/apt/ubuntu $(lsb_release -cs)/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt update
sudo apt install -yq mongodb-org
sudo systemctl enable mongod
sudo systemctl stop mongod
sudo rm -rf /var/lib/mongodb/*
sudo rm -rf /etc/mongod.conf
sudo rm -rf /lib/systemd/system/mongod.service

sync
