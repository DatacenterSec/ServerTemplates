#!/bin/bash

set -e

PRIVATE_IPV4_ADDRESS=$(curl http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address)

cat << EOF | sudo tee /etc/mongod.conf
# mongod.conf

# Documentation: http://docs.mongodb.org/manual/reference/configuration-options/

storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true

net:
  port: 27017
  bindIp: 127.0.0.1,${PRIVATE_IPV4_ADDRESS}

systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

processManagement:
  timeZoneInfo: /usr/share/zoneinfo
EOF
