#!/bin/bash

set -e

mkdir -p /etc/consul
mkdir -p /etc/nomad
mkdir -p /etc/vault
mkdir -p /etc/inspec
mkdir -p /etc/iptables
