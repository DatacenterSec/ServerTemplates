#!/bin/bash
set -e

cat << EOF | sudo tee /run/gce-metadata
GCE_PROJECT_NAME=$(curl -s -H "Metadata-Flavor:Google" http://metadata.google.internal/computeMetadata/v1/project/project-id)
GCE_ZONE=$(curl -s -H "Metadata-Flavor:Google" http://metadata.google.internal/computeMetadata/v1/instance/zone | tr / ' ' | awk '{print $4}')
GCE_INSTANCE_NAME=$(curl -s -H "Metadata-Flavor:Google" http://metadata.google.internal/computeMetadata/v1/instance/name)
GCE_PRIVATE_IPV4_ADDRESS=$(curl -s -H "Metadata-Flavor:Google" http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/ip)
GCE_PUBLIC_IPV4_ADRESS=$(curl -s -H "Metadata-Flavor:Google" http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)
EOF
